use {crate::sys, std::ffi};

pub struct Logger;

static LOGGER: Logger = Logger;

impl Logger {
    pub fn init() -> Result<(), log::SetLoggerError> {
        log::set_logger(&LOGGER).map(|()| log::set_max_level(log::LevelFilter::Trace))
    }
}

fn log_level_to_ml_log_level(level: log::Level) -> sys::MLLogLevel {
    match level {
        log::Level::Error => sys::MLLogLevel_Error,
        log::Level::Warn => sys::MLLogLevel_Warning,
        log::Level::Info => sys::MLLogLevel_Info,
        log::Level::Debug => sys::MLLogLevel_Debug,
        log::Level::Trace => sys::MLLogLevel_Verbose,
    }
}

impl log::Log for Logger {
    fn enabled(&self, metadata: &log::Metadata) -> bool {
        let level = log_level_to_ml_log_level(metadata.level());
        unsafe { sys::MLLoggingLogLevelIsEnabled(level) }
    }

    fn log(&self, record: &log::Record) {
        let level = log_level_to_ml_log_level(record.level());
        let tag = ffi::CString::new(record.module_path().unwrap_or_default()).unwrap();
        let message = ffi::CString::new(format!("{}", record.args())).unwrap();
        unsafe {
            sys::MLLoggingLog(level, tag.as_ptr(), message.as_ptr());
        }
    }

    fn flush(&self) {}
}
