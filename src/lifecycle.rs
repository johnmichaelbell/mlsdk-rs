use {
    crate::sys,
    std::{ffi, marker, os::raw, pin::Pin, ptr, sync::mpsc},
};

pub struct Lifecycle {
    sender: Pin<Box<mpsc::Sender<Event>>>,
    receiver: mpsc::Receiver<Event>,
}

// TODO: handle multiple initializations
impl Lifecycle {
    pub fn init() -> Self {
        let (sender, receiver) = mpsc::channel();
        let res = Lifecycle {
            sender: Box::pin(sender),
            receiver,
        };

        let callbacks = sys::MLLifecycleCallbacksEx {
            version: 2,
            on_stop: Some(on_stop),
            on_pause: Some(on_pause),
            on_resume: Some(on_resume),
            on_unload_resources: Some(on_unload_resources),
            on_new_initarg: Some(on_new_initarg),
            on_device_active: Some(on_device_active),
            on_device_reality: Some(on_device_reality),
            on_device_standby: Some(on_device_standby),
        };
        unsafe {
            sys::MLLifecycleInitEx(&callbacks, &*res.sender as *const _ as *mut _);
        }

        res
    }

    pub fn set_ready_indication(&self) {
        unsafe {
            sys::MLLifecycleSetReadyIndication();
        }
    }

    pub fn events(&self) -> &mpsc::Receiver<Event> {
        &self.receiver
    }

    pub fn init_args(&self) -> InitArgIter {
        InitArgIter {
            list: ptr::null_mut(),
            i: 0,
            len: 0,
            phantom: marker::PhantomData,
        }
    }
}

pub struct InitArgIter<'a> {
    list: *mut sys::MLLifecycleInitArgList,
    i: i64,
    len: i64,
    phantom: marker::PhantomData<&'a sys::MLLifecycleInitArgList>,
}

impl<'a> Iterator for InitArgIter<'a> {
    type Item = &'a str;

    fn next(&mut self) -> Option<Self::Item> {
        // First, see if we must aquire the next init arg list.
        if self.i == self.len {
            self.i = 0;
            if !self.list.is_null() {
                unsafe { sys::MLLifecycleFreeInitArgList(&mut self.list) };
            }
            unsafe {
                sys::MLLifecycleGetInitArgList(&mut self.list);
                sys::MLLifecycleGetInitArgListLength(self.list, &mut self.len);
            }
        }

        // Second, return the next init arg if there is one.
        if self.len == 0 {
            None
        } else {
            let mut init_arg: *const sys::MLLifecycleInitArg = std::ptr::null();
            let mut uri: *const raw::c_char = std::ptr::null();
            unsafe {
                sys::MLLifecycleGetInitArgByIndex(self.list, self.i, &mut init_arg);
                sys::MLLifecycleGetInitArgUri(init_arg, &mut uri);

                let cstr = ffi::CStr::from_ptr(uri);
                self.i += 1;
                cstr.to_str().ok()
            }
        }
    }
}

impl Drop for InitArgIter<'_> {
    fn drop(&mut self) {
        unsafe { sys::MLLifecycleFreeInitArgList(&mut self.list) };
    }
}

#[derive(Debug)]
pub enum Event {
    OnStop,
    OnPause,
    OnResume,
    OnUnloadResources,
    OnNewInitarg,
    OnDeviceActive,
    OnDeviceReality,
    OnDeviceStandby,
}

fn send_event(context: *mut raw::c_void, event: Event) {
    let sender: &mpsc::Sender<Event> = unsafe { std::mem::transmute(context) };
    if let Err(err) = sender.send(event) {
        log::error!("{}", err);
    }
}

extern "C" fn on_stop(context: *mut raw::c_void) {
    send_event(context, Event::OnStop)
}

extern "C" fn on_pause(context: *mut raw::c_void) {
    send_event(context, Event::OnPause)
}

extern "C" fn on_resume(context: *mut raw::c_void) {
    send_event(context, Event::OnResume)
}

extern "C" fn on_unload_resources(context: *mut raw::c_void) {
    send_event(context, Event::OnUnloadResources)
}

extern "C" fn on_new_initarg(context: *mut raw::c_void) {
    send_event(context, Event::OnNewInitarg)
}

extern "C" fn on_device_active(context: *mut raw::c_void) {
    send_event(context, Event::OnDeviceActive)
}

extern "C" fn on_device_reality(context: *mut raw::c_void) {
    send_event(context, Event::OnDeviceReality)
}

extern "C" fn on_device_standby(context: *mut raw::c_void) {
    send_event(context, Event::OnDeviceStandby)
}
