use {
    crate::sys,
    std::{ptr, slice},
};

pub struct Sound {
    handle: sys::MLHandle,
}

impl Default for Sound {
    fn default() -> Self {
        let mut handle = 0;
        let mut format = sys::MLAudioBufferFormat::default();
        let mut recommended_size = 0;
        unsafe {
            sys::MLAudioGetOutputStreamDefaults(
                2,
                48000,
                1.0,
                &mut format,
                &mut recommended_size,
                ptr::null_mut(),
            );
            sys::MLAudioCreateSoundWithOutputStream(
                &format,
                recommended_size,
                None,
                ptr::null_mut(),
                &mut handle,
            );
        }

        Self { handle }
    }
}

impl Drop for Sound {
    fn drop(&mut self) {
        unsafe { sys::MLAudioDestroySound(self.handle) };
    }
}

impl Sound {
    pub fn start(&mut self) {
        unsafe { sys::MLAudioStartSound(self.handle) };
    }

    pub fn stop(&mut self) {
        unsafe { sys::MLAudioStopSound(self.handle) };
    }

    // TODO: maybe make a scoped RAII type like MutexGuard to represent the buffer.
    pub fn fill<F: FnMut(&mut [u8])>(&mut self, mut fill_func: F) -> bool {
        let mut buffer = sys::MLAudioBuffer::default();
        let result = unsafe { sys::MLAudioGetOutputStreamBuffer(self.handle, &mut buffer) };
        if result == sys::MLAudioResult_BufferNotReady {
            return false;
        }
        unsafe {
            let buffer = slice::from_raw_parts_mut(buffer.ptr.cast(), buffer.size as _);
            fill_func(buffer);

            sys::MLAudioReleaseOutputStreamBuffer(self.handle);
        }

        true
    }
}
