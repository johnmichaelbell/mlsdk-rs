#![allow(nonstandard_style)]
#![allow(dead_code)]
#![allow(clippy::all)]

include!(concat!(env!("OUT_DIR"), "/mlsdk_bindings.rs"));
