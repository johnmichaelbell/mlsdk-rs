extern crate bindgen;

use std::env;
use std::path::Path;

fn main() {
    let out_dir = env::var("OUT_DIR").expect("Could not read OUT_DIR environment variable");
    let mlsdk_var = env::var("MLSDK").expect("Could not read MLSDK environment variable");
    let target_os = env::var("CARGO_CFG_TARGET_OS")
        .expect("Could not read CARGO_CFG_TAGET_OS environment variable");

    let platform = match target_os.as_ref() {
        "android" => "lumin",
        "linux" => "linux64",
        "windows" => "win64",
        "macos" => "osx",
        _ => panic!("device not supported"),
    };

    let clang_sysroot = match target_os.as_ref() {
        "android" => format!("--sysroot={}/lumin", mlsdk_var).to_owned(),
        _ => "".to_owned(),
    };

    bindgen::Builder::default()
        .header("mlsdk.h")
        .clang_arg(format!("-I{}/include", mlsdk_var))
        .clang_arg(clang_sysroot)
        .derive_default(true)
        .prepend_enum_name(false)
        .generate()
        .expect("Unable to generate bindings")
        .write_to_file(&Path::new(&out_dir).join("mlsdk_bindings.rs"))
        .expect("Couldn't write mlsdk bindings!");

    println!(
        "cargo:rustc-link-search=native={}/lib/{}",
        mlsdk_var, platform
    );

    println!("cargo:rustc-link-lib=dylib=ml_app_analytics");
    println!("cargo:rustc-link-lib=dylib=ml_audio");
    println!("cargo:rustc-link-lib=dylib=ml_camera_metadata");
    println!("cargo:rustc-link-lib=dylib=ml_camera");
    println!("cargo:rustc-link-lib=dylib=ml_dispatch");
    println!("cargo:rustc-link-lib=dylib=ml_ext_logging");
    println!("cargo:rustc-link-lib=dylib=ml_graphics");
    println!("cargo:rustc-link-lib=dylib=ml_graphics_utils");
    println!("cargo:rustc-link-lib=dylib=ml_identity");
    println!("cargo:rustc-link-lib=dylib=ml_input");
    println!("cargo:rustc-link-lib=dylib=ml_lifecycle");
    println!("cargo:rustc-link-lib=dylib=ml_mediacodeclist");
    println!("cargo:rustc-link-lib=dylib=ml_mediacodec");
    println!("cargo:rustc-link-lib=dylib=ml_mediacrypto");
    println!("cargo:rustc-link-lib=dylib=ml_mediadrm");
    println!("cargo:rustc-link-lib=dylib=ml_mediaerror");
    println!("cargo:rustc-link-lib=dylib=ml_mediaextractor");
    println!("cargo:rustc-link-lib=dylib=ml_mediaformat");
    println!("cargo:rustc-link-lib=dylib=ml_mediaplayer");
    println!("cargo:rustc-link-lib=dylib=ml_musicservice_provider");
    println!("cargo:rustc-link-lib=dylib=ml_musicservice");
    println!("cargo:rustc-link-lib=dylib=ml_perception_client");
    println!("cargo:rustc-link-lib=dylib=ml_platform");
    println!("cargo:rustc-link-lib=dylib=ml_privileges");
    println!("cargo:rustc-link-lib=dylib=ml_purchase");
    println!("cargo:rustc-link-lib=dylib=ml_screens");
    println!("cargo:rustc-link-lib=dylib=ml_secure_storage");
    println!("cargo:rustc-link-lib=dylib=ml_sharedfile");
}
