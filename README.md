# MLSDK Rust integration
This repository contains wrapper and example crates for using the MLSDK in rust

# Building and Running

## Prerequisites
- Rust version 1.33.0 or later: https://www.rust-lang.org/learn/get-started.

- The `aarch64-linux-android` toolchain for rust, which can be installed with `rustup`:
```bash
rustup target add aarch64-linux-android
```

- A recent version of the Lumin SDK. This can be downloaded with the Magic Leap Package Manager: https://creator.magicleap.com/downloads/lumin-sdk/overview

- A Magic Leap developer certificate, which can be obtained by following this guide: https://creator.magicleap.com/learn/guides/developer-certificates
NOTE: it is recommended that you create an `MLCERT` environment variable, so you can sign every package without needing to passing `-s path/to/certificate` to the Magic Leap Builder.

## Environment setup
The following commands must only be run once in a shell session in order to set the environment variables that cargo will use to build these crates.
```bash
export MLSDK=$HOME/MagicLeap/mlsdk/v0.20.0
source $MLSDK/envsetup.sh
source lumin_build_setup.sh
```

## Building and Running the Examples
### Targeting Magic Leap One
```bash
# Build the examples
cargo build --target aarch64-linux-android --examples
# Package the examples into an .mpk file
mabu examples/examples.package -t lumin
# Install the package onto your lumin device
mldb install -u target/aarch64-linux-android/debug/examples/examples.mpk
# Launch an app
mldb launch com.magicleap.mlsdk_rs.examples simple-gl-app
```

You can also package, install, and launch in a single command:
```bash
mabu examples/examples.package -t lumin --invoke
```

To build a release build instead of a debug build:
```bash
# Build the examples in release mode
cargo build --target aarch64-linux-android --release  --examples
# Package the examples into an .mpk file
mabu examples/examples.package -t release_lumin
```

### targeting Host Desktop using Magic Leap Remote
Currently not supported

# Contributing
mlsdk-rs welcomes feedback, suggestions, bug reports, pull requests, and any other form of contribution from everybody. In order to provide a welcoming and open environment, please be sure to follow the [Code of Conduct](CODE-OF-CONDUCT.md).
