extern crate gl_generator;
extern crate failure;

use gl_generator::{Api, Fallbacks, Profile, Registry, StaticGenerator, StructGenerator};
use std::env;
use std::fs::File;
use std::path::Path;

fn main() -> Result<(), failure::Error>  {
    let out_dir = env::var("OUT_DIR")?;

    let mut file = File::create(&Path::new(&out_dir).join("gl_bindings.rs"))?;
    Registry::new(Api::Gl, (4, 5), Profile::Core, Fallbacks::All, [])
        .write_bindings(StructGenerator, &mut file)?;

    let mut file = File::create(&Path::new(&out_dir).join("egl_bindings.rs"))?;
    Registry::new(Api::Egl, (1, 5), Profile::Core, Fallbacks::All, [])
        .write_bindings(StaticGenerator, &mut file)?;
    println!("cargo:rustc-link-lib=dylib=EGL");
    Ok(())
}
