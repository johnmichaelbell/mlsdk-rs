# Direct cargo to use the MLSDK's clang
export CARGO_TARGET_AARCH64_LINUX_ANDROID_LINKER="$MLSDK/tools/toolchains/bin/aarch64-linux-android-clang"
# Use the MLSDK's sysroot as well
export CARGO_TARGET_AARCH64_LINUX_ANDROID_RUSTFLAGS="-C link-args=--sysroot=$MLSDK/lumin"
# For dependencies require the cc crate, such as backtrace-rs.
export CC_aarch64_linux_android=$MLSDK/tools/toolchains/bin/aarch64-linux-android-clang
export AR_aarch64_linux_android=$MLSDK/tools/toolchains/bin/aarch64-linux-android-ar
export CFLAGS_aarch64_linux_android=--sysroot=$MLSDK/lumin