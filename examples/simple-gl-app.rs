mod support;

use {
    self::support::{egl, gl, gl::types::*},
    ml::sys,
    mlsdk_rs as ml,
    std::{ffi, os::raw, ptr, sync::atomic, time},
};

static IS_STOPPED: atomic::AtomicBool = atomic::AtomicBool::new(false);

fn main() -> Result<(), failure::Error> {
    ml::logging::Logger::init().expect("Could not init logger");
    let graphics_context = support::GraphicsContext::new();
    graphics_context.make_current();
    let gl = gl::Gl::load_with(|sym| {
        let sym = ffi::CString::new(sym.as_bytes()).unwrap();
        unsafe { egl::GetProcAddress(sym.as_ptr()) as *const raw::c_void }
    });

    let lifecycle_callbacks = sys::MLLifecycleCallbacks {
        on_stop: Some(on_lifecycle_stop),
        on_pause: None,
        on_resume: None,
        on_unload_resources: None,
        on_new_initarg: None,
    };
    let mut perception_settings = sys::MLPerceptionSettings::default();
    let graphics_options = sys::MLGraphicsOptions {
        graphics_flags: 0,
        color_format: sys::MLSurfaceFormat_RGBA8UNorm,
        depth_format: sys::MLSurfaceFormat_D32Float,
    };
    let mut graphics_client = 0;
    unsafe {
        into_result(sys::MLLifecycleInit(&lifecycle_callbacks, ptr::null_mut()))?;
        into_result(sys::MLPerceptionInitSettings(&mut perception_settings))?;
        into_result(sys::MLPerceptionStartup(&mut perception_settings))?;
        log::info!("creating client");
        into_result(sys::MLGraphicsCreateClientGL(
            &graphics_options,
            graphics_context.egl_context as sys::MLHandle,
            &mut graphics_client,
        ))?;
        log::info!("client created");
        into_result(sys::MLLifecycleSetReadyIndication())?;
    }

    let mut gl_framebuffer = 0;
    unsafe { gl.GenFramebuffers(1, &mut gl_framebuffer) };

    let start_time = time::Instant::now();
    log::info!("Starting render loop");
    while !IS_STOPPED.load(atomic::Ordering::Relaxed) {
        let mut frame_params = sys::MLGraphicsFrameParams::default();
        unsafe { into_result(sys::MLGraphicsInitFrameParams(&mut frame_params))? };
        let mut frame_handle = 0;
        let mut virtual_camera_array = sys::MLGraphicsVirtualCameraInfoArray::default();
        unsafe {
            let result = sys::MLGraphicsBeginFrame(
                graphics_client,
                &frame_params,
                &mut frame_handle,
                &mut virtual_camera_array,
            );
            if result != sys::MLResult_Ok as sys::MLResult {
                continue;
            }
        };

        let delta_time = start_time.elapsed();
        let millis = 1000 * delta_time.as_secs() as i64 + i64::from(delta_time.subsec_millis());
        let color_factor = (millis % 2000 - 1000).abs() as f32 / 1000.0;
        for camera_idx in 0..2 {
            unsafe {
                gl.BindFramebuffer(gl::FRAMEBUFFER, gl_framebuffer);
                gl.FramebufferTextureLayer(
                    gl::FRAMEBUFFER,
                    gl::COLOR_ATTACHMENT0,
                    virtual_camera_array.color_id as GLuint,
                    0,
                    camera_idx,
                );
                gl.FramebufferTextureLayer(
                    gl::FRAMEBUFFER,
                    gl::DEPTH_ATTACHMENT,
                    virtual_camera_array.depth_id as GLuint,
                    0,
                    camera_idx,
                );

                let viewport = virtual_camera_array.viewport;
                gl.Viewport(
                    viewport.x as GLint,
                    viewport.y as GLint,
                    viewport.w as GLsizei,
                    viewport.h as GLsizei,
                );
                if camera_idx == 0 {
                    gl.ClearColor(1.0 - color_factor, 0.0, 0.0, 1.0);
                } else {
                    gl.ClearColor(0.0, 0.0, color_factor, 1.0);
                }

                gl.Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

                gl.BindFramebuffer(gl::FRAMEBUFFER, 0);
                into_result(sys::MLGraphicsSignalSyncObjectGL(
                    graphics_client,
                    virtual_camera_array.virtual_cameras[camera_idx as usize].sync_object,
                ))?;
            }
        }

        unsafe { into_result(sys::MLGraphicsEndFrame(graphics_client, frame_handle))? };
    }
    log::info!("Stopping render loop");

    graphics_context.unmake_current();
    unsafe {
        gl.DeleteFramebuffers(1, &gl_framebuffer);
        into_result(sys::MLGraphicsDestroyClient(&mut graphics_client))?;
        into_result(sys::MLPerceptionShutdown())?;
    }

    Ok(())
}

extern "C" fn on_lifecycle_stop(_context: *mut raw::c_void) {
    IS_STOPPED.store(true, atomic::Ordering::Relaxed);
}

pub fn into_result(result: sys::MLResult) -> Result<(), failure::Error> {
    if result == sys::MLResult_Ok as sys::MLResult {
        Ok(())
    } else {
        Err(failure::format_err!("{}", result))
    }
}
