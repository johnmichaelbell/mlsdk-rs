pub mod egl;
pub mod gl;

use self::egl::types::*;
use std::ptr;

pub struct GraphicsContext {
    pub egl_display: EGLDisplay,
    pub egl_context: EGLContext,
}

impl GraphicsContext {
    pub fn new() -> Self {
        unsafe {
            let egl_display = egl::GetDisplay(egl::DEFAULT_DISPLAY);

            let mut major = 4;
            let mut minor = 0;
            egl::Initialize(egl_display, &mut major, &mut minor);
            egl::BindAPI(egl::OPENGL_API);

            #[cfg_attr(rustfmt, rustfmt_skip)]
            let config_attribs = [
                egl::RED_SIZE as egl::EGLint, 5,
                egl::GREEN_SIZE as egl::EGLint, 6,
                egl::BLUE_SIZE as egl::EGLint, 5,
                egl::ALPHA_SIZE as egl::EGLint, 0,
                egl::DEPTH_SIZE as egl::EGLint, 24,
                egl::STENCIL_SIZE as egl::EGLint, 8,
                egl::NONE as egl::EGLint,
            ];
            let mut egl_config = ptr::null();
            let mut config_size = 0;
            egl::ChooseConfig(
                egl_display,
                config_attribs.as_ptr(),
                &mut egl_config,
                1,
                &mut config_size,
            );

            #[cfg_attr(rustfmt, rustfmt_skip)]
            let context_attribs = [
                egl::CONTEXT_MAJOR_VERSION as EGLint, 4,
                egl::CONTEXT_MINOR_VERSION as EGLint, 5,
                egl::NONE as EGLint,
            ];
            let egl_context = egl::CreateContext(
                egl_display,
                egl_config,
                egl::NO_CONTEXT,
                context_attribs.as_ptr(),
            );

            GraphicsContext {
                egl_display,
                egl_context,
            }
        }
    }

    pub fn make_current(&self) {
        unsafe {
            egl::MakeCurrent(
                self.egl_display,
                egl::NO_SURFACE,
                egl::NO_SURFACE,
                self.egl_context,
            )
        };
    }

    pub fn unmake_current(&self) {
        unsafe { egl::MakeCurrent(ptr::null(), egl::NO_SURFACE, egl::NO_SURFACE, ptr::null()) };
    }
}

impl Drop for GraphicsContext {
    fn drop(&mut self) {
        unsafe {
            egl::DestroyContext(self.egl_display, self.egl_context);
            egl::Terminate(self.egl_display);
        }
    }
}
