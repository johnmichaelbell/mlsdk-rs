#![allow(non_camel_case_types)]
#![allow(clippy::all)]

use std::os::raw;

pub type khronos_utime_nanoseconds_t = raw::c_int;
pub type khronos_uint64_t = u64;
pub type khronos_ssize_t = isize;
pub type EGLNativeDisplayType = *const raw::c_void;
pub type EGLNativePixmapType = *const raw::c_void;
pub type EGLNativeWindowType = *const raw::c_void;
pub type EGLint = raw::c_int;
pub type NativeDisplayType = *const raw::c_void;
pub type NativePixmapType = *const raw::c_void;
pub type NativeWindowType = *const raw::c_void;
include!(concat!(env!("OUT_DIR"), "/egl_bindings.rs"));
